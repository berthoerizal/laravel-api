<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return response()->json([
            'status' => true,
            'message' => 'success get roles',
            'data' => $roles
        ]);
    }

    public function show($id)
    {
        $role = Role::find($id);
        return response()->json([
            'status' => true,
            'message' => 'success get role by id',
            'data' => $role
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'guestaccount'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::findOrFail($id);

        if ($role) {

            $role->update([
                'guestaccount'     => $request->guestaccount
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'guestaccount'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'guestaccount'     => $request->guestaccount,
        ]);

        if ($role) {

            return response()->json([
                'success' => true,
                'message' => 'success add role',
                'data'    => $role
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'failed add role',
        ], 409);
    }

    public function destroy($id)
    {
        $role = Role::findOrfail($id);

        if ($role) {

            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
