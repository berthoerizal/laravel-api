<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::all();
        return response()->json([
            'status' => true,
            'message' => 'success get comments',
            'data' => $comments
        ]);
    }

    public function show($id)
    {
        $comment = Comment::find($id);
        return response()->json([
            'status' => true,
            'message' => 'success get comment by id',
            'data' => $comment
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($id);

        if ($comment) {

            $comment->update([
                'content'     => $request->content,
                'post_id' => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content'     => $request->content,
            'post_id' => $request->post_id
        ]);

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'success add comment',
                'data'    => $comment
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'failed add comment',
        ], 409);
    }

    public function destroy($id)
    {
        $comment = Comment::findOrfail($id);

        if ($comment) {

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
